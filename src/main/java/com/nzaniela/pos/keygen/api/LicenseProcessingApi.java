/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nzaniela.pos.keygen.api;

import com.naftap.pos.keygen.LicenseManagerAPI;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import com.naftap.pos.keygen.display.api.Parsing;
import global.namespace.fun.io.api.Store;
import static global.namespace.fun.io.bios.BIOS.file;
import static global.namespace.fun.io.bios.BIOS.memory;
import static global.namespace.fun.io.bios.BIOS.stdin;
import static global.namespace.fun.io.bios.BIOS.stdout;
import global.namespace.truelicense.api.License;
import global.namespace.truelicense.api.LicenseManagerParameters;
import global.namespace.truelicense.api.VendorLicenseManager;
import global.namespace.truelicense.api.codec.Codec;
import static java.lang.System.err;
import static java.lang.System.out;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 *
 * @author nzaniela
 */
public enum LicenseProcessingApi {

    USAGE {
        @Override
        void run(final Deque<String> params) { throw new IllegalArgumentException(); }
    },

    HELP {
        @Override
        void run(final Deque<String> params) { out.printf(LicenseProcessingApi.valueOf(params.pop().toUpperCase(Locale.ROOT)).help()); }
    },

    VERSION {
        @Override
        void run(final Deque<String> params) { out.printf(message("version"), LicenseProcessingApi.class.getSimpleName()); }
    },

    GENERATE {
        @Override
        void run(final Deque<String> params) throws Exception {
            new Object() {

                final Map<CreateOption, String> options = Parsing.parse(params, CreateOption.class);
                VendorLicenseManager manager;
                LicenseManagerParameters parameters;

                {
                    final String licenseKeyPath = params.poll();
                    if (null != licenseKeyPath) {
                        options.put(CreateOption.KEY, licenseKeyPath);
                    }
                    Parsing.parse(params, CreateOption.class, options);
                    if (!params.isEmpty()) {
                        throw new IllegalArgumentException();
                    }
                }

                void run() throws Exception {
                    final License input = createOrDecodeInputLicense();
                    final License output = manager()
                            .generateKeyFrom(input)
                            .saveTo(store())
                            .license();
                    if (verbose()) {
                        err.println(output);
                    }
                    maybeEncodeOutputLicense(output);
                }

                License createOrDecodeInputLicense() throws Exception {
                    final String path = options.get(CreateOption.INPUT);
                    return null == path
                            ? parameters().licenseFactory().license()
                            : (License) codec()
                                .decoder("-".equals(path) ? stdin() : file(path))
                                .decode(License.class);
                }

                void maybeEncodeOutputLicense(final License license) throws Exception {
                    final String path = options.get(CreateOption.OUTPUT);
                    if (null != path) {
                        codec() .encoder("-".equals(path) ? stdout() : file(path))
                                .encode(license);
                    }
                }

                Codec codec() {
                    return parameters().codec();
                }

                LicenseManagerParameters parameters() {
                    final LicenseManagerParameters p = parameters;
                    return null != p ? p : (parameters = manager().parameters());
                }

                VendorLicenseManager manager() {
                    final VendorLicenseManager m = manager;
                    return null != m ? m : (manager = LicenseManagerAPI.valueOf(editionName()));
                }

                Store store() {
                    final String path = options.get(CreateOption.KEY);
                    return null == path ? memory() : file(path); // send to /dev/null if no path
                }

                String editionName() {
                    final String name = options.get(CreateOption.EDITION);
                    if (null != name) {
                        return name;
                    }
                    final LicenseManagerAPI[] managers = LicenseManagerAPI.values();
                    if (1 != managers.length) {
                        throw new IllegalArgumentException();
                    }
                    return managers[0].name();
                }

                boolean verbose() {
                    final String value = options.get(CreateOption.VERBOSE);
                    return null != value ? Boolean.parseBoolean(value) : false;
                }
            }.run();
        }
    };

    /**
     * Runs this command.
     * Implementations are free to modify the given deque.
     *
     * @param params the command parameters.
     */
    abstract void run(Deque<String> params) throws Exception;
    Parsing parsing  = new Parsing();
    @SuppressWarnings("CallToThreadDumpStack")
    public  int licenseProcessHandler(final String... args) {
        int status;
        try {
            licenseProcess(args);
            status = 0;
        } catch (final IllegalArgumentException | NoSuchElementException ex) {
            printUsage();
            status = 1;
        } catch (final Exception ex) {
            status = 2;
        }
        return status;
    }

    public  void licenseProcess(final String... args) throws Exception {
        final Deque<String> params = new LinkedList<>(Arrays.asList(args));
        final String command = Parsing.upperCase(params.pop());
        valueOf(command).run(params);
    }
    
    private  void printUsage() {
        final StringBuilder builder = new StringBuilder(25 * 80);
        for (final LicenseProcessingApi main : values()) {
            builder.append(main.usage());
        }
        err.println(builder.toString());
    }
    
    private String usage() {
        return String.format(message("usage"), LicenseProcessingApi.class.getSimpleName());
    }

    private String help() {
        return message("help");
    }

    String message(String key) {
        Locale currentLocale = new Locale("en", "US");
        String msg = "";
        ResourceBundle rsd = ResourceBundle
                .getBundle(LicenseProcessingApi.class.getName(), currentLocale,
                        this.getClass().getClassLoader());
        msg = rsd.getString(name() + "." + key);
        return msg;
    }
    
private enum CreateOption { KEY, INPUT, OUTPUT, EDITION, VERBOSE }

    
}
