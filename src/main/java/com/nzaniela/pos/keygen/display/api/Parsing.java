/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nzaniela.pos.keygen.display.api;

import static java.lang.Enum.valueOf;
import static java.lang.System.err;
import java.util.Deque;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 *
 * @author nzaniela
 */
public class Parsing {
     public static String upperCase(String s) { return s.toUpperCase(Locale.ENGLISH); }



    /**
     * Parses the given command parameters for options of the given class with
     * a string parameter.
     * As a side effect, any found options are popped off the parameter stack.
     *
     * @param  <T> the type of the enum class for the options.
     * @param  params the command parameters.
     * @param  type the enum class for the options.
     * @return an enum map with the options and parameters found.
     */
    public static <T extends Enum<T>> EnumMap<T, String> parse(
            final Deque<String> params,
            final Class<T> type) {
        return parse(params, type, new EnumMap<T, String>(type));
    }

    public static <T extends Enum<T>, M extends Map<T, String>> M parse(
            final Deque<String> params,
            final Class<T> type,
            final M options) {
        for (   String param;
                null != (param = params.peek()) && '-' == param.charAt(0);
                ) {
            params.pop(); // consume
            param = upperCase(param.substring(1));
            options.put(valueOf(type, param), params.pop());
        }
        return options;
    }

    private enum CreateOption { KEY, INPUT, OUTPUT, EDITION, VERBOSE }
    
}
