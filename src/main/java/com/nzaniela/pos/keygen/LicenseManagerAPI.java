/*
 * Copyright (C) 2005 - 2019 Schlichtherle IT Services.
 * All rights reserved. Use is subject to license terms.
 */
/* Generated from Velocity template at Mar 22, 2020 6:09:08 PM - DO NOT EDIT! */
package com.nzaniela.pos.keygen;

import global.namespace.truelicense.api.*;
import global.namespace.truelicense.api.passwd.PasswordProtection;
import global.namespace.truelicense.core.passwd.ObfuscatedPasswordProtection;
import global.namespace.truelicense.obfuscate.*;

import global.namespace.truelicense.v2.json.V2Json;

import javax.security.auth.x500.X500Principal;

/**
 * The enumeration of the vendor license managers for Unicenta POS (Naftap LLC) license keys.
 * The managers are named like each configured edition and ordered from
 * superset to subset.
 * Each manager is configured with the algorithms and parameters for generating
 * license keys for the respective edition.
 * <p>
 * This class is immutable and hence trivially thread-safe.
 *
 * @author Daniel M
 */
public enum LicenseManagerAPI implements VendorLicenseManager {

    enterprise {
        @Override
        VendorLicenseManager newManager() {
            return _managementContext
                    .vendor()
                    .encryption()
                        .protection(protection(new long[] { 0xdd95af6e7730397L, 0xbe396c4e5c431a1aL, 0x72370c32a5a2658bL }) /* => "daniel789?" */)
                        .up()
                    .authentication()
                        .alias(name())
                        .loadFromResource(KEY_STORE_FILE)
                        .storeProtection(protection(new long[] { 0x7298664aab69bc97L, 0x8e16ef9e8dc68249L, 0x4ea0a502fccfa863L }) /* => "daniel789?" */)
                        .up()
                    .build();
        }
    };

    @Obfuscate
    private static final String DISTINGUISHED_NAME = "CN=Nais Technologies Inc.";

    @Obfuscate
    private static final String KEY_STORE_FILE = "private.ks";

    @Obfuscate
    private static final String SUBJECT = "Unicenta POS (Naftap LLC)";

    private static final LicenseManagementContext _managementContext = V2Json
            .builder()
            .initialization(bean -> {
                if (null == bean.getIssuer()) {
                    bean.setIssuer(new X500Principal(DISTINGUISHED_NAME));
                }
            })
            .subject(SUBJECT)
            .build();

    private volatile VendorLicenseManager _manager;

    private VendorLicenseManager manager() {
        // No need to synchronize because managers are virtually stateless.
        final VendorLicenseManager m = _manager;
        return null != m ? m : (_manager = newManager());
    }

    abstract VendorLicenseManager newManager();

    private static PasswordProtection protection(long[] obfuscated) {
        return new ObfuscatedPasswordProtection(new ObfuscatedString(obfuscated));
    }

    @Override
    public LicenseManagerParameters parameters() { return manager().parameters(); }

    @Override
    public LicenseKeyGenerator generateKeyFrom(License bean) throws LicenseManagementException {
        return manager().generateKeyFrom(bean);
    }
}
